package edu.spring.jdbc;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.spring.jdbc.dto.BoardDto;
import edu.spring.jdbc.service.BoardService;

public class MainJdbc {
	public static void main(String[] args) {
		ApplicationContext con = new ClassPathXmlApplicationContext("edu/spring/jdbc/jdbc-config.xml");

//		MemberService ms = (MemberService) con.getBean("memberService");
//		MemberDto memberDto = new MemberDto();
//		memberDto.setId("aaa");
//		memberDto.setPw("11");
//		memberDto.setName("가가");

//		ms.addMember(memberDto);

//		MemberService ms = (MemberService) con.getBean("memberService");
//		MemberDto memberDto = new MemberDto();
//		List<MemberDto> list1 = ms.getMemberList1(memberDto);
//		for (MemberDto dto : list1) {
//			System.out.println(dto.getId() + " / " + dto.getName());
//		}
//		List<Map<String, Object>> list2 = ms.getMemberList2();
//		for (Map<String, Object> m : list2) {
//			System.out.println(m.get("m_id") + " / " + m.get("M_NAME"));
//		}
//		
//		MemberDto memberDto3 = new MemberDto();
//		memberDto3.setId("aa");
//		memberDto3.setName("하하");
//		ms.modifyMember(memberDto3);
		
		BoardService ms = (BoardService) con.getBean("boardService");
		BoardDto boardDto = new BoardDto();
		boardDto.setbId(2);
		boardDto.setmId("aa");
		boardDto.setbTitle("첫번째 제목2");
		boardDto.setbContent("첫번째 내용2");
//		ms.addBoard(boardDto);
		
		List<Map<String, Object>> list = ms.getBoardList2();
		for(int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
	}
}











