package edu.spring.jdbc;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.spring.jdbc.dto.MemberDto;
import edu.spring.jdbc.service.MemberService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:edu/spring/jdbc/jdbc-config.xml"})
public class MemberServiceTest {
	@Autowired
	MemberService ms;
	
	@Test
	public void test() {
		MemberDto memberDto = new MemberDto();
		List<MemberDto> list1 = ms.getMemberList1(memberDto);
		for (MemberDto dto : list1) {
			System.out.println(dto.getId() + " / " + dto.getName());
		}
		List<Map<String, Object>> list2 = ms.getMemberList2();
		for (Map<String, Object> m : list2) {
			System.out.println(m.get("m_id") + " / " + m.get("M_NAME"));
		}
	}

}
