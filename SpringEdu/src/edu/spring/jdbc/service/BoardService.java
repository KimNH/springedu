package edu.spring.jdbc.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.spring.jdbc.dao.BoardDao;
import edu.spring.jdbc.dto.BoardDto;

@Service
public class BoardService {
	@Autowired
	BoardDao boardDao;

	public int addBoard(BoardDto boardDto) {
		return boardDao.insertBoard(boardDto);
	}

	public List<BoardDto> getBoardList1(BoardDto boardDto) {
		return boardDao.selectBoardToDto(boardDto);
	}

	public List<Map<String, Object>> getBoardList2() {
		return boardDao.selectBoardToMap();
	}

	public int modifyBoard(BoardDto boardDto) {
		return boardDao.updateBoard(boardDto);
	}

}







