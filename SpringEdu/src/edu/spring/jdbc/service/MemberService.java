package edu.spring.jdbc.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.spring.jdbc.dao.MemberDao;
import edu.spring.jdbc.dto.MemberDto;

@Service
public class MemberService {
	@Autowired
	MemberDao memberDao;

	public int addMember(MemberDto memberDto) {
		return memberDao.insertMember(memberDto);
	}

	public List<MemberDto> getMemberList1(MemberDto memberDto) {
		return memberDao.selectMemberToDto(memberDto);
	}

	public List<Map<String, Object>> getMemberList2() {
		return memberDao.selectMemberToMap();
	}

	public int modifyMember(MemberDto memberDto) {
		return memberDao.updateMember(memberDto);
	}

}







