package edu.spring.jdbc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import edu.spring.jdbc.dto.MemberDto;

@Repository
public class MemberDao {
	@Autowired
	JdbcTemplate jdbcTemplate;

	public int insertMember(MemberDto memberDto) {
		String sql = "INSERT INTO JDBC_MEMBER VALUES (?, ?, ?, SYSDATE())";
		return jdbcTemplate.update(sql, memberDto.getId(), memberDto.getPw(), memberDto.getName());
	}

	public List<MemberDto> selectMemberToDto(MemberDto memberDto) {
		String sql = "SELECT M_ID, M_PW, M_NAME, CRE_DATE" + " FROM JDBC_MEMBER";
		return jdbcTemplate.query(sql, new Object[] {}, new RowMapper<MemberDto>() {
			@Override
			public MemberDto mapRow(ResultSet rs, int rowNum) throws SQLException {
				MemberDto resultDto = new MemberDto();
				resultDto.setId(rs.getString("m_id"));
				resultDto.setName(rs.getString("M_NAME"));
				return resultDto;
			}
		});
	}

	public List<Map<String, Object>> selectMemberToMap() {
		String sql = "SELECT M_ID, M_PW" +
	                 "     , M_NAME, CRE_DATE" +
	                 "  FROM JDBC_MEMBER";
		return jdbcTemplate.queryForList(sql);
	}

	public int updateMember(MemberDto memberDto) {
		String sql = "UPDATE JDBC_MEMBER SET" + 
	                 "       M_NAME = ?" + 
				     " WHERE M_ID = ?";
		return jdbcTemplate.update(
				sql, memberDto.getName(), 
				memberDto.getId());
	}

}




