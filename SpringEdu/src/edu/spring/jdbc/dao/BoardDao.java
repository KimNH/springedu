package edu.spring.jdbc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import edu.spring.jdbc.dto.BoardDto;
import edu.spring.jdbc.dto.MemberDto;

@Repository
public class BoardDao {
	@Autowired
	JdbcTemplate jdbcTemplate;

	public int insertBoard(BoardDto boardDto) {
		String sql = "INSERT INTO JDBC_BOARD VALUES " +
	                 "      (?, ?, ?, ?, 0, NOW())";
		return jdbcTemplate.update(sql, boardDto.getbId(), 
				boardDto.getmId(), boardDto.getbTitle(),
				boardDto.getbContent());
	}

	public List<BoardDto> selectBoardToDto(BoardDto boardDto) {
		String sql = "SELECT B_ID, M_ID, B_TITLE, B_CONTENT" +
	                 "     , B_HIT, CRE_DATE" + 
	                 " FROM JDBC_BOARD";
		return jdbcTemplate.query(sql, new Object[] {}, new RowMapper<BoardDto>() {
			@Override
			public BoardDto mapRow(ResultSet rs, int rowNum) throws SQLException {
				BoardDto resultDto = new BoardDto();
				resultDto.setbId(rs.getInt("B_ID"));
				resultDto.setbTitle(rs.getString("B_TITLE"));
				resultDto.setbContent(rs.getString("B_CONTENT"));
				resultDto.setHit(rs.getInt("B_HIT"));
				resultDto.setCreDate(rs.getString("CRE_DATE"));
				return resultDto;
			}
		});
	}

	public List<Map<String, Object>> selectBoardToMap() {
		String sql = "SELECT B_ID, M_ID, B_TITLE" +
	                 "     , B_CONTENT, B_HIT, CRE_DATE" +
	                 "  FROM JDBC_BOARD";
		return jdbcTemplate.queryForList(sql);
	}

	public int updateBoard(BoardDto boardDto) {
		String sql = "UPDATE JDBC_BOARD SET" + 
	                 "       B_TITLE = ?" + 
				     " WHERE B_ID = ?";
		return jdbcTemplate.update(
				sql, boardDto.getbTitle(), 
				boardDto.getbId());
	}

}




