package edu.spring.jdbc;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.spring.jdbc.dto.MemberDto;
import edu.spring.jdbc.service.MemberService;

public class MemberTest {

	@Test
	public void readMember() {
		ApplicationContext con = 
				new ClassPathXmlApplicationContext(
						"edu/spring/jdbc/jdbc-config.xml");

		MemberService ms = (MemberService) con.getBean("memberService");
		MemberDto memberDto = new MemberDto();
		List<MemberDto> list1 = ms.getMemberList1(memberDto);
		for (MemberDto dto : list1) {
			System.out.println(dto.getId() + " / " + dto.getName());
		}
		List<Map<String, Object>> list2 = ms.getMemberList2();
		for (Map<String, Object> m : list2) {
			System.out.println(m.get("m_id") + " / " + m.get("M_NAME"));
		}

	}
	
	@Test
	public void addMember() {
		ApplicationContext con = new ClassPathXmlApplicationContext("edu/spring/jdbc/jdbc-config.xml");

		MemberService ms = (MemberService) con.getBean("memberService");
		MemberDto memberDto = new MemberDto();
		memberDto.setId("aaa");
		memberDto.setPw("11");
		memberDto.setName("����");

		ms.addMember(memberDto);
	}

}






