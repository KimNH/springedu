package edu.spring.context;

public interface Emp {
	int age = 100; // public static final int age = 100;
	
	void gotoOffice();

	void getOffWork();
}
