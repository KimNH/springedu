package edu.spring.context;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainContext {
	public static void main(String[] args) {
		ApplicationContext con = 
		new ClassPathXmlApplicationContext("edu/spring/context/context-config.xml");
		Develop dev = (Develop) con.getBean("develop");
		dev.coding();
	}
}