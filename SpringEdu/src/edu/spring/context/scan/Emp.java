package edu.spring.context.scan;

public interface Emp {
	int age = 100; // public static final int age = 100;
	
	void gotoOffice();

	void getOffWork();
}
