package edu.spring.context;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Develop {
//	@Autowired
//	@Qualifier("designer")
	@Resource(name="programmer")
	private Emp emp;

	public void coding() {
		emp.gotoOffice();
		System.out.println("회사에서 일하는 중");
		emp.getOffWork();
	}
}