package edu.spring.aop.xml;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainAop {
	public static void main(String[] args) {
		ApplicationContext con = 
		new ClassPathXmlApplicationContext(
				"edu/spring/aop/xml/aop-config.xml");
		Develop dev = (Develop) con.getBean("develop");
		dev.coding();
	}
}