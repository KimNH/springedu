package edu.spring.aop.xml;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

@Service
public class Develop {
//	@Autowired
//	@Qualifier("designer")
	@Resource(name="programmer")
	private Emp emp;

	public void coding() {
		emp.gotoOffice();
		System.out.println("회사에서 일하는 중");
		emp.getOffWork();
	}
}