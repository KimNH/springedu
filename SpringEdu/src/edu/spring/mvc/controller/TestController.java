package edu.spring.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TestController {
	@RequestMapping("/first.do")
	public String first(Model model) {
		model.addAttribute("data", "First Data");
		return "first";
	}

	@RequestMapping("/second")
	public ModelAndView second() {
		ModelAndView mav = new ModelAndView();
		mav.addObject("data", "Second Data");
		mav.setViewName("second");
		return mav;
	}
}



