package edu.spring.bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainBean {
	public static void main(String[] args) {
		ApplicationContext con = new ClassPathXmlApplicationContext("edu/spring/bean/bean-config.xml");
		Develop dev = (Develop) con.getBean("develop");
		dev.coding();
	}
}