package edu.spring.transaction.java;

import java.sql.Connection;
import java.sql.SQLException;

public class BuyService {
	public void buy(String memberId, int productId) {
		Connection conn = null;
		try {
			conn = Connector.getInstance().getConnection();
			conn.setAutoCommit(false);
			int result1 = new OrderDao().insertOrder(conn, memberId, productId);
			System.out.println("구매내역 추가 : " + result1);
			int result2 = new ProductDao().updateProduct(conn, productId);
			System.out.println("상품 개수 감소 : " + result2);
			if (result1 > 0 && result2 > 0) {
				conn.commit();
			} else {
				conn.rollback();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			/* conn close */
		}
	}
}