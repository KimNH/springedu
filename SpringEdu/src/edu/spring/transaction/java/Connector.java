package edu.spring.transaction.java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {
	private static Connector con = null;
	private String url = "jdbc:mysql://localhost:3306/spring";
	private String id = "user", pw = "654321";

	private Connector() {
	}

	public static synchronized Connector getInstance() {
		if (con == null)
			con = new Connector();
		return con;
	}

	public Connection getConnection() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(url, id, pw);
		return conn;
	}
}