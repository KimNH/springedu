package edu.spring.transaction.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class OrderDao {
	PreparedStatement pstmt = null;

	public int insertOrder(Connection conn, String memberId, int productId) {
		int result = 0;
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO JDBC_ORDER (O_ID, M_ID, P_ID) VALUES");
		sql.append(" (NULL, ?, ?)");
		
		try {
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setString(1, memberId);
			pstmt.setInt(2, productId);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {}
			}
		}
		return result;
	}
}