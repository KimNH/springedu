package edu.spring.transaction.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ProductDao {
	PreparedStatement pstmt = null;

	public int updateProduct(Connection conn, int productId) {
		int result = 0;
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE JDBC_PRODUCT SET ");
		sql.append(" P_COUNT = P_COUNT - 1");
		sql.append(" WHERE P_ID = ? AND P_COUNT > 0");
		try {
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setInt(1, productId);
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}
		}
		return result;
	}
}
